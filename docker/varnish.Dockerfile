FROM alpine:3.7

# Install Dependencies
RUN apk add --update varnish

# Copy over varish configs
COPY docker/varnish.vcl /etc/varnish/default.vcl

# Expose port 80 for varnish to listen to
EXPOSE 80

CMD ["varnishd", "-f", "/etc/varnish/default.vcl", "-a", "0.0.0.0:80", "-s", "malloc,1G", "-F"]
