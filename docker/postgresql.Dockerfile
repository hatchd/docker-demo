FROM ubuntu:18.04

ARG PG_VERSION

RUN groupadd -r postgres --gid=999 && useradd -r -g postgres --uid=999 postgres

RUN apt-get update
RUN apt-get -y upgrade

# Stop ubuntu asking for timezone
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install tzdata

RUN apt-get install -y postgresql-${PG_VERSION}

USER postgres

RUN /etc/init.d/postgresql start && createdb demo

RUN sed -i 's/md5/trust/g' /etc/postgresql/${PG_VERSION}/main/pg_hba.conf
RUN echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/${PG_VERSION}/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/${PG_VERSION}/main/postgresql.conf

EXPOSE 5432

VOLUME ["/var/lib/postgresql/data", "/var/lib/postgresql", "/var/log/postgresql", "/etc/postgresql"]

ENTRYPOINT exec /usr/lib/postgresql/$PG_VERSION/bin/postgres -D /var/lib/postgresql/$PG_VERSION/main -c config_file=/etc/postgresql/$PG_VERSION/main/postgresql.conf
