FROM alpine:3.7

# Install Dependencies
# linux-headers: uwsgi
RUN apk add --update git python3 postgresql-dev python3-dev gcc musl-dev linux-headers

# Update pip and install virtualenv for python
RUN pip3 install --upgrade pip uwsgi psycopg2

# Copy over files to our application directory
COPY . /opt/application

ARG EXAMPLE_VARIABLE
ENV EXAMPLE_VARIABLE $EXAMPLE_VARIABLE

# Expose port 8080 for varnish to listen to the app
EXPOSE 8080

CMD ["uwsgi", "--http", ":8080", "--py-autoreload", "1", "--wsgi-file", "/opt/application/application.py", "--master", "--processes", "4", "--threads", "2"]
