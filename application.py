import psycopg2
import logging
import os


def application(env, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    dsn = "host='db' dbname='demo' user='postgres'"
    try:
        conn = psycopg2.connect(dsn)
        cursor = conn.cursor()
        message = (
            "Success! You are connected to PostgreSQL! <br /><br />"
            f"Environment supplied by Docker-Compose: '{os.environ['EXAMPLE_VARIABLE']}'".encode()
        )
        return [message]
    except Exception as e:
        message = (
            f"Could not connect to the postgresql database using {dsn}: {e} <br /><br />"
            f"Environment supplied by Docker-Compose: '{os.environ['EXAMPLE_VARIABLE']}'".encode()
        )
        logger = logging.getLogger(__name__)
        logger.exception(message)
        return [message]
